# Federated social identity backup and restore

## Current version

0.1.0 (released 7th Feb 2016)

See: https://the-federation.info/specs/backup-restore/0.1.0.html

## Latest published version

See: https://the-federation.info/specs/backup-restore
